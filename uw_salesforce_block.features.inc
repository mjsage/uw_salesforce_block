<?php
/**
 * @file
 * uw_salesforce_block.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_salesforce_block_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_salesforce_block_field_default_field_instances_alter(&$data) {
  if (isset($data['node-uw_ct_single_page_home-field_sph_blocks'])) {
    $data['node-uw_ct_single_page_home-field_sph_blocks']['settings']['allowed_bundles']['webform_block'] = 'webform_block'; /* WAS: '' */
    $data['node-uw_ct_single_page_home-field_sph_blocks']['settings']['bundle_weights']['webform_block'] = 26; /* WAS: '' */
  }
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_salesforce_block_paragraphs_info() {
  $items = array(
    'webform_block' => array(
      'name' => 'Webform Block',
      'bundle' => 'webform_block',
      'locked' => '1',
    ),
  );
  return $items;
}
