<?php
/**
 * @file
 * uw_salesforce_block.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_salesforce_block_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.node-uw_ct_single_page_home-field_sph_blocks.settings|allowed_bundles|webform_block"] = 'webform_block';
  $overrides["field_instance.node-uw_ct_single_page_home-field_sph_blocks.settings|bundle_weights|webform_block"] = 26;

 return $overrides;
}
